<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Films extends Model
{
    //

    protected $table = 'films';
    protected $fillable = ['judul', 'ringkasan', 'tahun', 'poster', 'genre_id'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    public function genre()
    {
        return $this->belongsTo('App\Genres');
    }

}
